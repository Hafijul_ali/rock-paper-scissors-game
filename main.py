from backend.game import get_move, get_result
from quart import Quart, jsonify, render_template

BASE = "frontend"
TEMPLATES = f"{BASE}/templates"
STATIC = f"{BASE}/static"

app = Quart(__name__, static_folder=STATIC, template_folder=TEMPLATES)

@app.route("/")
@app.route("/<self_move>")
async def index(self_move=''):
    if self_move is not None:
        other_player_move = get_move()
        filepath = f'assets/{other_player_move}.png'
        message = get_result(self_move, other_player_move)
        return await render_template('index.html', result=[message,filepath])
    return await render_template('index.html', result=['Ready','assets/ready.png'])

