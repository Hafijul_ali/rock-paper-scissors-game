from random import choice


def get_move():
    moves = ["rock", "paper", "scissors"]
    move = choice(moves)
    return move


def get_result(first_player, second_player):
	if first_player == second_player:
		return "Its a Draw"
	elif first_player == "rock" and second_player == "scissors":
		return "You Won"
	elif first_player == "scissors" and second_player == "paper":
		return "You Won"
	elif first_player == "paper" and second_player == "rock":
		return "You Won"
	return None
